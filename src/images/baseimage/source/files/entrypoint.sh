#!/usr/bin/env bash
USER_NAME=${USER_NAME:-ubuntu}
USER_ID=${LOCAL_USER_ID:-9001}

# test user exists
if id $USER_NAME > /dev/null 2>&1; then
  usermod -u $USER_ID $USER_NAME
else
  useradd --shell /bin/bash -u $USER_ID -o -c "" -m $USER_NAME
fi

export HOME=/home/$USER_NAME

# copy skeleton files manually
# mucking about with UIDS seems to cause useradd to 
# not both copying skeleton files
cp /etc/skel/.* $HOME

# set permissions
chown -R $USER_ID $HOME

echo "Starting with UID : $USER_ID"
exec /usr/local/bin/gosu $USER_NAME "$@"
