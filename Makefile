.PHONY: clean build style

DOCKER_USER ?= ''
DOCKER_PASS ?= ''

TARGET_DIR ?= "$(shell pwd)/target"
ARTIFACT_DIR ?= "$(TARGET_DIR)/artifacts"

.EXPORT_ALL_VARIABLES:

setup:
	@mkdir -p $(ARTIFACT_DIR)

clean:
	@set -e
	@for line in `cat $(ARTIFACT_DIR)/images.txt`; \
	do \
		docker rmi $$line; \
	done
	@rm -rf $(TARGET_DIR)
	@docker images -q -f 'dangling=true' | xargs --no-run-if-empty docker rmi

build: setup
	@set -e
	@for image in ./src/images/*; \
	do \
		echo "building $$image"; \
		cd $$image; \
		make build; \
		cd ../../..; \
	done

style: setup
	set -e
	@for image in ./src/images/*; \
	do \
		cd $$image; \
		make style; \
		cd ../../..; \
	done

$(ARTIFACT_DIR)/images.txt:

deploy: $(ARTIFACT_DIR)/images.txt
	@set -e
	@docker login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	@for line in `cat $(ARTIFACT_DIR)/images.txt`; \
  do \
		docker push $$line; \
  done
