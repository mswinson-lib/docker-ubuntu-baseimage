# ubuntu-baseimage

    ubuntu baseimage

## Tags


## Usage

run with default user (ubuntu)

    docker run -it --rm mswinson/ubuntu-baseimage:latest

run with local user id

    docker run -it --rm -e LOCAL_USER_ID=`id -u $USER` mswinson/ubuntu-baseimage:latest

run with user name

    docker run -it --rm -e LOCAL_USER_ID=`id -u $USER` -e USER_NAME=developer mswinson/ubuntu-baseimage:latest

## Image variants

### mswinson/ubuntu-baseimage:<release>

from  

    mswinson/ubuntu:<release>

entrypoint  

    creates and runs user with local user id

### mswinson/ubuntu-baseimage:<release>-x11-client

from  

    mswinson/ubuntu-baseimage:<release>

packages

    libsdl2

    xauth
    x11-apps
    xterm

